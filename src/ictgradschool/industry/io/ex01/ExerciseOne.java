package ictgradschool.industry.io.ex01;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ExerciseOne {

    public void start() {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.
        try (FileReader fR = new FileReader(new File("input2.txt"))) {

            int num;

            while ((num = fR.read()) != -1) {

                total++;
                char letter = (char)num;
                if (letter == 'e' || letter == 'E') {
                    numE++;
                }
            }

        } catch (IOException e){
                System.out.println(e);
        }
        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }


    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.
        File myFile = new File("input2.txt");
        try (BufferedReader reader = new BufferedReader(new FileReader(myFile))) {

            String line = null;
            while ((line = reader.readLine()) != null) {
                for (int i = 0; i<line.length(); i++){
                    total++;
                    char letter = line.charAt(i);
                    if (letter == 'e' || letter == 'E') {
                        numE++;
                    }
                }
            }
        } catch (IOException e) {
            System.out.println(e);

        }
            System.out.println("Number of e/E's: " + numE + " out of " + total);
        }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
