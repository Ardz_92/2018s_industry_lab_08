package ictgradschool.industry.io.ex04;

import ictgradschool.industry.io.ex03.Movie;
import ictgradschool.industry.io.ex03.MovieReader;

import java.io.File;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {
        Movie[] movies = new Movie[19];

        // TODO Implement this with a Scanner
        File f = new File(fileName);
        try (Scanner scanner = new Scanner(new File(fileName))) {
            int i = 0;
            scanner.useDelimiter(",|\\r\\n");

            while (scanner.hasNext()) {
                String movie = scanner.next();
                int year = scanner.nextInt();
                int length = scanner.nextInt();
                String director = scanner.next();
                movies[i] = new Movie(movie, year, length, director);
                i++;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return movies;
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}
