package ictgradschool.industry.io.ex04;

import ictgradschool.industry.io.ex03.Movie;
import ictgradschool.industry.io.ex03.MovieWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieWriter extends MovieWriter {

    @Override
    protected void saveMovies(String fileName, Movie[] films) {

        // TODO Implement this with a PrintWriter
        try (PrintWriter csvData = new PrintWriter(new File(fileName))) {
            for (int i = 0; i < films.length; i++) {
                csvData.append(films[i].getName());
                csvData.append(",");
                csvData.append(String.valueOf(films[i].getYear()));
                csvData.append(",");
                csvData.append(String.valueOf(films[i].getLengthInMinutes()));
                csvData.append(",");
                csvData.append(films[i].getDirector());
                csvData.println();
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }

        public static void main (String[]args){
            new Ex4MovieWriter().start();
        }

    }
